package spring.example.trading.service;

import java.util.Collection;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.example.trading.entities.Stocks;
import spring.example.trading.entities.TradeStatus;
import spring.example.trading.repo.StocksRepo;

@Service
public class StocksServiceImpl implements StocksService {

    @Autowired
    private StocksRepo repo;

    @Override
    public Collection<Stocks> getStocks() {
        return repo.findAll();
    }

    @Override
    public void addStocks(Stocks trade) {
        repo.insert(trade);
    }

    @Override
    public void updateStocks(ObjectId id, double quantity) {
        Optional<Stocks> trade = repo.findById(id);
        if (trade != null) {
            Stocks trade_old = trade.get();
            trade_old.setRequestedPrice(quantity);
            repo.save(trade_old);
        }
    }

    @Override
    public void deleteStocks(Stocks trade) {
        repo.delete(trade);
    }

    @Override
    public void deleteStocksById(ObjectId id) {
        repo.deleteById(id);

    }

    @Override
    public void deleteAll() {
        repo.deleteAll();

    }

    @Override
    public Optional<Stocks> getStocksById(ObjectId id) {
        return repo.findById(id);
    }

    @Override
    public Collection<Stocks> getStocksByTicker(String ticker) {
        return repo.findByTicker(ticker);
    }

    @Override
    public Collection<Stocks> getStocksByStatus(TradeStatus status) {
        return repo.findByStatus(status);
    }


    
}