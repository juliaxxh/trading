package spring.example.trading.service;

import java.util.Collection;

import spring.example.trading.entities.Stocks;
import spring.example.trading.entities.TradeStatus;

import org.bson.types.ObjectId;
import java.util.Optional;

public interface StocksService {

    Collection<Stocks> getStocks();
    void addStocks(Stocks trade);
    void updateStocks(ObjectId id,double quantity);
    void deleteStocks(Stocks trade);
    void deleteStocksById(ObjectId id);
    void deleteAll();

    Optional<Stocks> getStocksById(ObjectId id);

    Collection<Stocks> getStocksByTicker(String Ticker);
    Collection<Stocks> getStocksByStatus(TradeStatus state);
    
}