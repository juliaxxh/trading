package spring.example.trading.repo;

import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import spring.example.trading.entities.Stocks;
import spring.example.trading.entities.TradeStatus;

public interface StocksRepo extends MongoRepository<Stocks,ObjectId> 

{

    public Collection<Stocks> findByTicker(String ticker);

    public Collection<Stocks> findByStatus(TradeStatus status);
    
    List<Stocks> findByTickerContaining(String ticker);

    
}