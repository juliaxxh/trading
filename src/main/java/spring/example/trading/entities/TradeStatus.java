package spring.example.trading.entities;

public enum TradeStatus {
    CREATED("CREATED"),
    PROCESSING("PROCESSSING"),
    FILLED("FILLED"),
    REJECTED("REJECTED");

    private String state;

    private TradeStatus(String state) {
        this.state = state;
    }

    public String getStatus() {
        return this.state;
    } 
}

    