package spring.example.trading.controller;

import java.util.*;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import spring.example.trading.entities.Stocks;
import spring.example.trading.repo.StocksRepo;
import spring.example.trading.service.StocksService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("")
public class StocksController {

  @Autowired
  StocksRepo stocksRepository;

  @Autowired
  private StocksService service;

  @GetMapping("/stocks")
  public ResponseEntity<List<Stocks>> getAllstocks(@RequestParam(required = false) String ticker) {
    try {
      List<Stocks> stocks = new ArrayList<Stocks>();

      if (ticker == null)
        stocksRepository.findAll().forEach(stocks::add);
      else
        stocksRepository.findByTickerContaining(ticker).forEach(stocks::add);

      if (stocks.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(stocks, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/stocks/{id}")
  public ResponseEntity<Stocks> getstocksById(@PathVariable("id") ObjectId id) {
    Optional<Stocks> stocksData = stocksRepository.findById(id);

    if (stocksData.isPresent()) {
      return new ResponseEntity<>(stocksData.get(), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping("/stocks")
  public ResponseEntity<Stocks> createstocks(@RequestBody Stocks stocks) {
    try {
      Stocks _stocks = stocksRepository.save(new Stocks(stocks.getTicker(), stocks.getStockQuantity(), stocks.getRequestedPrice(),stocks.getType()));
      return new ResponseEntity<>(_stocks, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/stocks/{id}")
  public ResponseEntity<Stocks> updatestocks(@PathVariable("id") ObjectId id, @RequestBody Stocks stocks) {
    Optional<Stocks> stocksData = stocksRepository.findById(id);

    if (stocksData.isPresent()) {
      Stocks _stocks = stocksData.get();
      _stocks.setTicker(stocks.getTicker());
      _stocks.setRequestedPrice(stocks.getRequestedPrice());
      _stocks.setStockQuantity(stocks.getStockQuantity());
      return new ResponseEntity<>(stocksRepository.save(_stocks), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }
  @RequestMapping(method = RequestMethod.GET, value = "/trade/get/{id}")
	public Optional<Stocks> getTdById(@PathVariable("id") String id) {
		return service.getStocksById(new ObjectId(id));
	}

  @DeleteMapping("/stocks/{id}")
  public ResponseEntity<HttpStatus> deletestocks(@PathVariable("id") ObjectId id) {
    try {
      stocksRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @RequestMapping(method = RequestMethod.GET,value = "/stocks/ticker/{ticker}")
	@ResponseBody
    public Collection<Stocks> findByTicker(@PathVariable("ticker") String ticker){
       return service.getStocksByTicker(ticker);
	}

  @DeleteMapping("/stocks")
  public ResponseEntity<HttpStatus> deleteAllstocks() {
    try {
      stocksRepository.deleteAll();
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  @RequestMapping(method = RequestMethod.GET,value = "/stocks/find")
    public Iterable<Stocks> findAll() {
		return service.getStocks();
    }

  @GetMapping("/portfolio")
  public ResponseEntity<HashMap<String, Integer>> getPortfolio() {
    try {
      List<Stocks> stocks = new ArrayList<Stocks>(stocksRepository.findAll());
      if (stocks.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      // Add to hash map
      HashMap<String, Integer> portfolio = new HashMap<>();
      Map<String, Integer> buyOrSell = Map.of("BUY", 1, "SELL", -1);
      for (Stocks stock : stocks) {
        Integer buyOrSellQuantity = buyOrSell.get(stock.getType()) * stock.getStockQuantity();
        portfolio.merge(stock.getTicker(), buyOrSellQuantity, Integer::sum);
      }

      return new ResponseEntity<>(portfolio, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
