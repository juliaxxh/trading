package spring.example.trading.rest;

// import static org.hamcrest.Matchers.assertThat;
import static org.hamcrest.Matchers.hasSize;
// import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.List;

import spring.example.trading.entities.Stocks;
import spring.example.trading.service.StocksService;

import org.junit.Before;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

public class StocksControllerFunctionalTest {
    private RestTemplate template = new RestTemplate();

    @Test
    public void testFindAll(){
        @SuppressWarnings("unchecked")
        List<Stocks> trades=template.getForObject("http://localhost:8080/stocks", List.class);

        assertThat(trades, hasSize(1));
    }

    @Test
    public void testGetTradeById(){
        Stocks trade =template.getForObject("http://localhost:8080/stocks/5f63af96c82da16eb2236b49",Stocks.class);
        assertThat(trade, notNullValue());
    }

}
