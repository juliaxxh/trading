package spring.example.trading.rest;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;



import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import spring.example.trading.controller.StocksController;
import spring.example.trading.entities.Stocks;
import spring.example.trading.repo.StocksRepo;
import spring.example.trading.service.StocksService;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = StocksControllerUnitTest.Config.class)
public class StocksControllerUnitTest {

    public static final String TEST_ID = "5f46a3d545bee629d17fd7b2";

    @Configuration
    public static class Config {

        @Bean
        public StocksRepo repo() {
            return mock(StocksRepo.class);
        }

        @Bean
        public StocksService service() {

            ObjectId ID = new ObjectId(TEST_ID);
            Stocks trade = new Stocks("", 0, 0);
            List<Stocks> trades = new ArrayList<>();
            trades.add(trade);

            StocksService service = mock(StocksService.class);
            when(service.getStocks()).thenReturn(trades);
            when(service.getStocksById(ID)).thenReturn(Optional.of(trade));
            return service;
        }

        @Bean
        public StocksController controller() {
            return new StocksController();
        }
    }

    @Autowired
    private StocksController controller;

    @Test
    public void testFindAll() {
        Iterable<Stocks> trades = controller.findAll();
        Stream<Stocks> stream = StreamSupport.stream(trades.spliterator(), false);
        assertThat(stream.count(), equalTo(1L));
    }

    @Test
    public void testStocksById() {
        ResponseEntity<Stocks> trade = controller.getstocksById(new ObjectId(TEST_ID));
        assertNotNull(trade);
    }


}

