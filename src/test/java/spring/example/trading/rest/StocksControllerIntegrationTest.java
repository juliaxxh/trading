package spring.example.trading.rest;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import spring.example.trading.TradingApplication;
import spring.example.trading.controller.StocksController;
import spring.example.trading.entities.Stocks;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TradingApplication.class)
public class StocksControllerIntegrationTest {
    public static final String TEST_ID = "5f63af96c82da16eb2236b49"; // exist in the db

    @Autowired
    private StocksController controller;
    private Stocks trade = new Stocks(TEST_ID, "TSLA", 10, 5000);

    @Test
    public void testAddTd() {
        controller.deleteAllstocks();
        controller.createstocks(trade);
        ResponseEntity<Stocks> trade_test = controller.getstocksById(new ObjectId(TEST_ID));
        assertNotNull(trade_test);

    }

    



    @Test
    public void testFindAll() {
        controller.createstocks(trade);
        Iterable<Stocks> trades = controller.findAll();
        Stream<Stocks> stream = StreamSupport.stream(trades.spliterator(), false);
        assertThat(stream.count(), equalTo(1L));
    }
   
    @Test
    public void testGetStocksById() {
        controller.createstocks(trade);
        ResponseEntity<Stocks> trade = controller.getstocksById(new ObjectId(TEST_ID));
        assertNotNull(trade);
    }

    

    @Test
    public void testDeleteTd(){
        Stocks trade_sample = new Stocks("APPL",154,480);
        controller.createstocks(trade_sample);
        controller.deleteAllstocks();
        Iterable<Stocks> trades = controller.findAll();
        Stream<Stocks> stream = StreamSupport.stream(trades.spliterator(), false);
        assertThat(stream.count(), equalTo(0L));

    }

    @Test
    public void testDeleteTdById(){
        controller.deleteAllstocks();
        controller.createstocks(trade);
        Stocks trade_sample = new Stocks("APPL",1546,480);
        controller.createstocks(trade_sample);
        Iterable<Stocks> trades = controller.findAll();
        Stream<Stocks> stream = StreamSupport.stream(trades.spliterator(), false);
        assertThat(stream.count(), equalTo(2L));
        controller.deletestocks(new ObjectId(TEST_ID));
        Optional<Stocks> trade_res = controller.getTdById(TEST_ID);
        assertThat(trade_res.isPresent(), equalTo(false));

    }



    @Test
    public void testFindByTicker(){
        controller.deleteAllstocks();
        Stocks trade_sample1 = new Stocks("APPL",156,480);
        Stocks trade_sample2 = new Stocks("APPL",146,480);
        Stocks trade_sample3 = new Stocks("APPL",546,480);
        controller.createstocks(trade_sample1);
        controller.createstocks(trade_sample2);
        controller.createstocks(trade_sample3);
        controller.createstocks(trade);
        Iterable<Stocks> trades = controller.findByTicker("APPL");
        Stream<Stocks> stream = StreamSupport.stream(trades.spliterator(), false);
        assertThat(stream.count(), equalTo(3L));
        controller.deleteAllstocks();

    }


}
